![输入图片说明](https://git.oschina.net/uploads/images/2016/1229/000636_eaf9f510_134400.png "在这里输入图片标题")

### 声明
Discuz! L1.0 由Discuz粉丝基于Discuz! X系列开发，仅供 [Discuz! 粉丝网](https://www.discuzfans.net/) 用户进行学习交流，本团队不承担任何因使用本软件而产生问题的相关责任，若您将此程序用于生产环境，请自行承担风险，并遵守[Discuz官方](http://www.discuz.net/)的授权协议。

### Discuz! Lite安装包下载
[https://www.discuzfans.net/t-16661.html](https://www.discuzfans.net/t-16661.html)

### Discuz! X 官方 Git/SVN SC_UTF8 版本
[https://git.oschina.net/ComsenzDiscuz/DiscuzX](https://git.oschina.net/ComsenzDiscuz/DiscuzX)

### 相关网站

- Discuz! 官方站：http://www.discuz.net
- Discuz! 应用中心：http://addon.discuz.com
- Discuz! 开放平台：http://open.discuz.net
- Discuz! 粉丝网：https://www.discuzfans.net

![输入图片说明](https://git.oschina.net/uploads/images/2016/1229/002755_5e1b3227_134400.png "在这里输入图片标题")